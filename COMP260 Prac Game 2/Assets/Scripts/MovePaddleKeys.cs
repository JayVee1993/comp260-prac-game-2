﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddleKeys : MonoBehaviour {

	private Rigidbody rigidbody;
	public float speed = 20f;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		// get the input values
		float dirX = Input.GetAxis("Horizontal");
		float dirY = Input.GetAxis("Vertical");

		//velocity
		Vector3 movement = new Vector3 (dirX, dirY, 0);

		// move the object
		rigidbody.AddForce (movement * speed);
	
	}
}
