﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {

	private Rigidbody rigidbody;
	// Use this for initialization
	void Start () {
		
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;
	}

	private Vector3 GetMousePosition() {
		//creates a ray from the camera
		//passing through the mouse position
		Ray ray = 
			Camera.main.ScreenPointToRay(Input.mousePosition);

		//find out where the ray intersects the XY plane
		Plane plane = new Plane(Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}

	void OnDrawGizmos() {
		//draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition ();
		Gizmos.DrawLine (Camera.main.transform.position, pos);
	}
	
	// Move Paddle: Approach 1 - rigidbody.MovePosition
	/*void FixedUpdate () {
		Vector3 pos = GetMousePosition ();
		rigidbody.MovePosition (pos);
	}
	*/

	// Move Paddle: Approach 2 - Rigidbody.velocity
	public float speed = 20f;

	void FixedUpdate () {
		Vector3 pos = GetMousePosition ();
		Vector3 dir = pos - rigidbody.position;
		// Avoid overshoot
		Vector3 vel = dir.normalized * speed;

		// check is this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			// scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}

		rigidbody.velocity = vel;

		//dir = dir.normalized;
		//rigidbody.velocity = dir * speed;
	}


	//Move Paddle: Approach 3 - Rigidbody
	/*public float force =10f;

	void FixedUpdate () {
		Vector3 pos = GetMousePosition();
		Vector3 dir = pos - rigidbody.position;

		rigidbody.AddForce(dir.normalized * force);
	}
	*/
}
